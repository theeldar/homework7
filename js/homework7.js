function translate (sentance) {
    var translated = "";
    var vowel = 'aeiouyAEIOUY ';
    var letter;
    for (var i = 0; i < sentance.length; i++) {
        letter = sentance.substr(i,1);
        if (vowel.indexOf(letter) == -1) {
            translated = translated + letter + "o" + letter;
        } else translated += letter;
    }
    console.log(translated);
}

function sum (numbers) {
    var a = 0;
    for (var i = 0; i < numbers.length; i++) {
        a += numbers[i];
    }
    console.log("The sum is: " + a);
}

function multiply (numbers) {
    var a = 1;
    for (var i = 0; i < numbers.length; i++) {
        a *= numbers[i];
    }
    console.log("The multiplication is: " + a);
}
